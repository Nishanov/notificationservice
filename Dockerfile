# Используем базовый образ Ubuntu 20.04
FROM ubuntu:20.04

# Обновляем список пакетов и устанавливаем необходимые зависимости
RUN apt-get update && \
    apt-get install -y openssh-client && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Проверяем наличие ssh-agent и устанавливаем openssh в случае отсутствия
RUN command -v ssh-agent >/dev/null || ( apt-get update && apt-get install -y openssh-client && apt-get clean && rm -rf /var/lib/apt/lists/*)

# Активируем ssh-agent
RUN eval $(ssh-agent -s)
