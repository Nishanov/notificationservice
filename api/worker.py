import os
from dotenv import load_dotenv
from celery import Celery

load_dotenv()

CELERY_BROKER_URL = os.getenv("REDISSERVER", default="redis://localhost:6379")
CELERY_RESULT_BACKEND = os.getenv("REDISSERVER", default="redis://localhost:6379")
POSTGRES_USER = os.getenv('POSTGRES_USER')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD')
POSTGRES_DB = os.getenv('POSTGRES_DB')
POSTGRES_HOST = os.getenv('POSTGRES_HOST')
POSTGRES_PORT = os.getenv('POSTGRES_PORT')
TOKEN = os.getenv("TOKEN", default="")


celery = Celery("celery",
                backend=CELERY_BROKER_URL,
                broker=CELERY_RESULT_BACKEND)

