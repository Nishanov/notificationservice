from django.db import models


class Campaign(models.Model):
    id = models.AutoField(primary_key=True)
    start_time = models.DateTimeField()
    message_text = models.TextField()
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=100)
    end_time = models.DateTimeField()


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=11)  # Формат: 7XXXXXXXXXX
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=100)
    timezone = models.CharField(max_length=100)


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField()
    status = models.CharField(max_length=100)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
