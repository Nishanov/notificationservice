from rest_framework import generics
from .models import Campaign, Client, Message
from .serializers import CampaignSerializer, ClientSerializer, MessageSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Count
import datetime
import requests
import api.settings as settings
from worker import celery
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class CampaignListCreateView(generics.ListCreateAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer


class CampaignDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer


class ClientListCreateView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageListCreateView(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessageDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


# obtaining general statistics on the created mailing lists and the number of sent messages on them, grouped by status
class GeneralStatisticsView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_queryset(self):
        return Message.objects.values('campaign', 'status').annotate(count=Count('id')).order_by('campaign')


# obtaining detailed statistics of sent messages for a specific mailing list
class DetailedStatisticsView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_queryset(self):
        return Message.objects.filter(campaign=self.kwargs['campaign_id']).values('status').annotate(
            count=Count('id')).order_by('status')


class SendMessagesView(APIView):
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter('campaign_id', openapi.IN_PATH, description="Campaign ID", type=openapi.TYPE_INTEGER),
        ]
    )
    def post(self, request, campaign_id):
        campaign = Campaign.objects.get(id=campaign_id)
        if campaign.start_time <= datetime.datetime.now() <= campaign.end_time:
            clients = Client.objects.filter(mobile_operator_code=campaign.mobile_operator_code)
            for client in clients:
                message = Message()
                message.created_at = datetime.datetime.now()
                message.status = 'sent'
                message.campaign = campaign
                message.client = client
                message.save()

                task = celery.send_task('send_message', args=[message.id, client.phone, campaign.text])
                
                if task.status == 'FAILURE':
                    message.status = 'failed'
                    message.save()

                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)


class TestCeleryResultView(APIView):
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter('task_id', openapi.IN_QUERY, description="Task ID", type=openapi.TYPE_STRING),
        ]
    )
    def get(self, request, *args, **kwargs):
        task_id = request.query_params.get('task_id', None)
        task = celery.AsyncResult(task_id)
        return Response(status=status.HTTP_200_OK, data={'task_status': task.status, 'task_result': task.result})
