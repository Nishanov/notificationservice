from celery import states
from worker import *
from celery.schedules import crontab
from celery.utils.log import get_task_logger
import requests
import psycopg2


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(10.0, send_message_schedular.s(), name='send_message_schedular')


@celery.task(name='send_message', bind=True)
def send_message(self, message_id: int, phone: str, text: str) -> str:
    try:
        # sending messages to customers
        requests.post(f'https://probe.fbrq.cloud/v1/send/{message_id}',
                      json={
                          "id": message_id,
                          "phone": phone,
                          "text": text
                      },
                      headers={'Authorization': f'Bearer {worker.TOKEN}'})
    except requests.exceptions.RequestException as e:
        return states.FAILURE
    return states.SUCCESS


@celery.task(name='send_message_schedular', bind=True)
def send_message_schedular(self) -> str:
    conn = psycopg2.connect(dbname=POSTGRES_DB,
                            user=POSTGRES_USER,
                            password=POSTGRES_PASSWORD,
                            host=POSTGRES_HOST)
    cursor = conn.cursor()
    cursor.execute('SELECT id FROM api_campaign WHERE start_time < NOW() AND end_time > NOW()')
    campaigns = cursor.fetchall()
    for campaign in campaigns:
        requests.post('web:8000/send_message/', json={'campaign_id': campaign[0]})
    return states.SUCCESS
